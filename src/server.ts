import { NpocFrontendApplication } from "./application";
import { ApplicationConfig } from "@loopback/core";
import express from "express";
// import { Request, Response } from "express";
// import path from "path";
import pEvent from "p-event";
import http from "http";
import * as bodyParser from "body-parser";
import expressEjsLayouts from "express-ejs-layouts";
import session from "express-session";
import favicon from "serve-favicon";
import errorHandler from "./middlewares/errorHandler";
import router from "./routes";

export class ExpressServer {
  private app: express.Application;
  private lbApp: NpocFrontendApplication;
  private server?: http.Server;

  constructor(options: ApplicationConfig = {}) {
    /***** Loopback App *****/
    this.lbApp = new NpocFrontendApplication(options);

    /***** Express App *****/
    this.app = express();
    this.app.set("views", `${__dirname}/../views`);
    this.app.set("view engine", "ejs");
    this.app.use(expressEjsLayouts);
    this.app.set("layout extractScripts", true);
    this.app.set("layout", "layout/default");
    this.app.use(favicon(`${__dirname}/../public/favicon.ico`));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true, limit: '10mb', parameterLimit: 10000 }));
    this.app.use(
      session({
        secret: <string>process.env.SESSION_SECRET || "session_secret",
        resave: true,
        saveUninitialized: true
      })
    );

    this.app.use(express.static(`${__dirname}/../public`));

    // this.app.use(
    //   "/bootstrap",
    //   express.static(`${__dirname}/../node_modules/bootstrap/dist`)
    // );
    this.app.use(router);
    this.app.use(errorHandler);
  }

  async boot() {
    await this.lbApp.boot();
  }

  public async start() {
    // await this.lbApp.start();
    const port = process.env.PORT || 3001;
    this.server = this.app.listen(port);
    await pEvent(this.server, "listening");
    console.log(`Server is running on port: ${port}`);
    console.log(`API Endpoint : ${process.env.API_ENDPOINT}`);
  }

  // For testing purposes
  public async stop() {
    if (!this.server) return;
    await this.lbApp.stop();
    this.server.close();
    await pEvent(this.server, "close");
    this.server = undefined;
  }
}
