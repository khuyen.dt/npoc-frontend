/* eslint-disable @typescript-eslint/explicit-function-return-type */
/**
 * Sales service
 */
import {Service} from './common';
import * as model from 'npoc-model';
import {OK} from 'http-status';

export class SalesService extends Service {
  /**
   * GET /sales
   * @param params
   */
  public async salesSearch(params: {
    filter: {
      where: model.models.Customer & {
        pageNo: number;
        pageLine: number;
      };
    };
  }) {
    return this.fetch<model.specs.SearchResponse>({
      url: '/sales/',
      method: 'GET',
      params,
      expectedStatusCode: OK,
    });
  }

  /**
   * POST /sales
   * @param data
   */
  public async salesCreate(data: model.specs.SalesCreate) {
    return this.fetch<model.specs.CreateResponse>({
      url: '/sales/',
      method: 'POST',
      data,
      expectedStatusCode: OK,
    });
  }

  /**
   * POST /sales
   * @param data
   */
  public async salesUpdate(id: number | string, data: model.specs.SalesCreate) {
    return this.fetch<model.specs.CreateResponse>({
      url: `/sales/${id}`,
      method: 'PATCH',
      data,
      expectedStatusCode: OK,
    });
  }

  /**
   * GET /sales by id
   * @param params
   */
  public async salesSearchId(id: number | string) {
    return this.fetch<model.models.Sales>({
      url: `/sales/${id}`,
      method: 'GET',
      expectedStatusCode: OK,
    });
  }
}
