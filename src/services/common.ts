/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * Api service
 */
import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import {OK} from 'http-status';

export interface ITokenPair {
  accessToken: string;
  refreshToken?: string;
}

/**
 * service constructor options
 */
export interface IOptions {
  /**
   * API endpoint
   * @example
   * http://localhost:8081
   */
  endpoint: string;
  accessToken?: string;
  refreshToken?: string;
  storeToken?(token: ITokenPair): void;
  getAccessToken?(): Promise<ITokenPair>;
}

export interface IFetchOptions {
  url: string;
  method?: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH';
  data?: any;
  params?: any;
  expectedStatusCode: number;
  revoke?: boolean;
  noAuth?: boolean;
  headers?: any;
  filter?: any;
}

export class Service {
  private REVOKE_TOKEN_URL = '/user/refreshToken';
  private option: IOptions;
  constructor(option: IOptions) {
    this.option = option;
  }

  public async fetch<T>(fetchOption: IFetchOptions): Promise<T> {
    try {
      const axiosOption: AxiosRequestConfig = {
        baseURL: this.option.endpoint,
        url: fetchOption.url,
        method: fetchOption.method || 'GET', // default to GET
        data: fetchOption.data,
        validateStatus: (status: number) =>
          status === fetchOption.expectedStatusCode,
        params: fetchOption.params,
        headers: {},
      };
      // ログインの時、認証が必要ない
      if (fetchOption.noAuth !== true) {
        axiosOption.headers.Authorization = `Bearer ${this.option.accessToken}`;
      }
      if (fetchOption.headers !== undefined) {
        axiosOption.headers = {
          ...axiosOption.headers,
          ...fetchOption.headers,
        };
      }
      const response: AxiosResponse<T> = await axios(axiosOption);

      return response.data;
    } catch (err) {
      console.log(err);

      if (err.response === undefined) {
        err.response = {};
      }

      if (err.response.status === 422) {
        err.response.message = err.response.data.errors
          .map((e: any) => e.message)
          .join('\n');
      }

      throw err;

      // let token: ITokenPair | undefined;
      // // アクセストークンが失効される場合
      // if (
      //   err.response !== undefined &&
      //   err.response.status === UNAUTHORIZED &&     // TODO: Will fix using refreshToken
      //   fetchOption.revoke !== false
      // ) {
      //   token = await this.getAccessToken();

      //   if (this.option.storeToken !== undefined) {
      //     this.option.storeToken(token);
      //   }

      //   // 新しいトークンを保存して、次のリクエストで使う
      //   this.option.accessToken = token.accessToken;
      //   this.option.refreshToken = token.refreshToken;

      //   const newFetchOption = {
      //     ...fetchOption,
      //     revoke: false
      //   };

      //   // 再度リクエストする、失敗の場合、止まる
      //   return this.fetch<T>(newFetchOption);
      // } else {
      //   throw err;
      // }
    }
  }

  private async getAccessToken(): Promise<ITokenPair> {
    // if (this.option.refreshToken === undefined) {
    //   throw new Error('refresh token');
    // }

    if (this.option.getAccessToken !== undefined) {
      return this.option.getAccessToken();
    } else {
      const response = await axios.post<{token: ITokenPair}>(
        this.REVOKE_TOKEN_URL,
        {refreshToken: this.option.refreshToken},
        {
          baseURL: this.option.endpoint,
          validateStatus: status => status === OK,
        },
      );

      return response.data.token;
    }
  }
}
