/**
 * INDEX
 */
import {CustomerService} from './customer';
import {SalesService} from './sales';
import {UserService} from './user';

export namespace service {
  export class Customer extends CustomerService {}
  export class User extends UserService {}
  export class Sales extends SalesService {}
}
