/* eslint-disable @typescript-eslint/explicit-function-return-type */
/**
 * CustomerService
 */
import {Service} from './common';
import * as model from 'npoc-model';
import {OK} from 'http-status';

export class CustomerService extends Service {
  /**
   * POST /customer
   * @param data
   */
  public async customerCreate(data: model.specs.CustomerCreate) {
    return this.fetch<model.specs.CreateResponse>({
      url: '/customer/',
      method: 'POST',
      data,
      expectedStatusCode: OK,
    });
  }

  /**
   * GET /customer
   * @param params
   */
  public async customerSearch(params: {
    filter: {
      where: model.models.Customer & {
        pageNo: number;
        pageLine: number;
      };
    };
  }) {
    return this.fetch<model.specs.SearchResponse>({
      url: '/customer/',
      method: 'GET',
      params,
      expectedStatusCode: OK,
    });
  }

  /**
   * GET /customer by id
   * @param params
   */
  public async customerSearchId(id: number | string) {
    return this.fetch<model.models.Customer>({
      url: `/customer/${id}`,
      method: 'GET',
      expectedStatusCode: OK,
    });
  }
}
