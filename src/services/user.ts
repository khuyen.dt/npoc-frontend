/* eslint-disable @typescript-eslint/explicit-function-return-type */
/**
 * ユーザーサービス
 */
import {Service} from './common';
import * as model from 'npoc-model';
import {CREATED, OK} from 'http-status';

export class UserService extends Service {
  /**
   * ログイン
   * @param params ログイン情報
   * @returns トークン、ユーザ情報
   */
  public async login(params: model.models.LoginUser) {
    return this.fetch<{
      token: string;
      user: model.models.User;
    }>({
      url: '/user/login',
      method: 'POST',
      data: params,
      expectedStatusCode: OK,
      noAuth: true,
    });
  }

  /**
   * ユーザーログアウト
   * @param refreshToken トークン
   * @returns 成功？
   */
  public async logout(refreshToken: string) {
    return this.fetch<{}>({
      url: '/user/userLogout',
      method: 'POST',
      data: {refreshToken},
      expectedStatusCode: OK,
    });
  }

  /**
   * ユーザー検索
   * @param params 検索条件
   * @returns ユーザー情報
   */
  public async userSearch(params: model.models.User) {
    return this.fetch<model.specs.SearchResponse>({
      url: '/user/',
      method: 'GET',
      params,
      expectedStatusCode: OK,
    });
  }

  /**
   * ユーザー登録
   * @param data ユーザー情報
   */
  public async userCreate(data: model.models.User) {
    return this.fetch<model.specs.CreateResponse>({
      url: '/user/',
      method: 'POST',
      data,
      expectedStatusCode: CREATED,
    });
  }

  /**
   * ユーザーID検索
   * @params ユーザーID
   * @returns ユーザー情報
   */
  public async userSearchId(id: number) {
    return this.fetch<model.models.User>({
      url: `/user/${id}`,
      method: 'GET',
      expectedStatusCode: OK,
    });
  }

  /**
   * ユーザー編集
   * @param data ユーザー情報
   */
  public async userUpdate(data: model.models.User) {
    return this.fetch<model.specs.CreateResponse>({
      url: `/user/${data.id}`,
      method: 'PUT',
      data,
      expectedStatusCode: OK,
    });
  }
}
