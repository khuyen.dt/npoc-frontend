/**
 * middlewares/authenticationにて、expressのrequestオブジェクトにAPIユーザー情報を追加している。
 * ユーザーの型をここで定義しています。
 */
import * as express from "express";
import { IMessage } from "../../constants";
import * as model from "npoc-model";

declare global {
  namespace Express {
    // tslint:disable-next-line:interface-name
    interface Request {
      user: Partial<model.models.User> & {
        token?: {
          accessToken: string;
          refreshToken: string;
        };
        isAuthorized: boolean;
        getServiceOption(): {
          endpoint: string;
          accessToken?: string;
          refreshToken?: string;
          storeToken?(token: {
            accessToken: string;
            refreshToken: string;
          }): void;
        };
        destroy(): void;
      };
      consumeSession<X>(): { formData?: X; message?: IMessage };
    }
  }
}
