/**
 * Login Router
 */
import {Router} from 'express';
import * as customerController from '../controllers/customer.controller';
import viewHelper from '../middlewares/viewHelper';
import * as bodyParser from '../bodyParsers';

const customerRouter = Router();

customerRouter.use(viewHelper);
customerRouter.get('/', customerController.index);
customerRouter.get('/add', customerController.add);
customerRouter.post(
  '/add',
  bodyParser.Customer.CustomerAdd,
  customerController.addProcess,
);

export default customerRouter;
