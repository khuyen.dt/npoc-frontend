/**
 * Login Router
 */
import {Router} from 'express';
import * as userController from '../controllers/user.controller';

const authRouter = Router();

authRouter.get('/login', userController.login);
authRouter.post('/login', userController.auth);
authRouter.get('/logout', userController.logout);

export default authRouter;
