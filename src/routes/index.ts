/**
 * Main Router
 */
import {Router} from 'express';
import {notFound as notFoundHandler} from '../controllers/error.controller';
import auth from '../middlewares/authentication';
import sessionMiddleWare from '../middlewares/session';
import userMiddleware from '../middlewares/user';
import apiRouter from './api';
import authRouter from './auth';
import customerRouter from './customer';
import salesRouter from './sales';

const router = Router();

router.use(sessionMiddleWare);
router.use(userMiddleware);
router.use('/', authRouter);
router.use(auth);
router.use('/customer', customerRouter);
router.use('/sales', salesRouter);
router.use('/web/api', apiRouter);

router.get('/', (req, res) => {
  res.render('index', {
    user: req.user,
  });
});

// 404 error
router.use(notFoundHandler);

export default router;
