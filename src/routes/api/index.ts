/**
 * 担当部署 API Router
 */
import { Router } from 'express';
import customerApiRouter from './customer';

const apiRouter = Router();

apiRouter.use((_, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  next();
});

apiRouter.use('/customer', customerApiRouter);

export default apiRouter;
