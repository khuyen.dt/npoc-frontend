/**
 * Login Router
 */
import { Router } from 'express';
import * as customerController from '../../controllers/api/customer.controller';

const customerApiRouter = Router();

customerApiRouter.get('/', customerController.getCustomer);

export default customerApiRouter;
