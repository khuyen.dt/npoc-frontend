/**
 * Login Router
 */
import {Router} from 'express';
import * as salesController from '../controllers/sales.controller';
import viewHelper from '../middlewares/viewHelper';
import * as bodyParser from '../bodyParsers';

const salesRouter = Router();

salesRouter.use(viewHelper);
salesRouter.get('/', salesController.index);
salesRouter.get('/add', salesController.add);
salesRouter.get('/add/customer/:customerId([0-9]+)', salesController.add);
salesRouter.post('/add', bodyParser.Sales.SalesAdd, salesController.addProcess);
salesRouter.post(
  '/add/customer/:customerId([0-9]+)',
  bodyParser.Sales.SalesAdd,
  salesController.addProcess,
);

salesRouter.get('/:id([0-9]+)/edit', salesController.edit);
salesRouter.post('/:id([0-9]+)/edit', salesController.editProcess);

export default salesRouter;
