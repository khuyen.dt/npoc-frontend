/**
 * 認証ミドルウェア
 */

import {NextFunction, Request, Response} from 'express';

export default async (req: Request, res: Response, next: NextFunction) => {
  if (!req.user.isAuthorized) {
    res.redirect(`/login?redirect=${encodeURIComponent(req.originalUrl)}`);
  } else {
    next();
  }
};
