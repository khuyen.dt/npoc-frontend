/**
 * Error Handler Middleware
 */

// import * as createDebug from 'debug';
import {Request, Response} from 'express';
import {BAD_REQUEST, NOT_FOUND, UNAUTHORIZED} from 'http-status';
import * as model from 'npoc-model';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const commonErrorProcess = (err: any, req: Request, res: Response) => {
  let status = BAD_REQUEST;
  if (err.response !== undefined) {
    status = err.response.status;
  }

  if (status === UNAUTHORIZED) {
    res.redirect(`/logout?redirect=${encodeURIComponent(req.originalUrl)}`);
    return;
  }
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default (err: any, req: Request, res: Response) => {
  // debug(err.message);
  let title = model.constants.messages.UNEXPECTED_ERROR;
  let error = '';
  let status = BAD_REQUEST;
  if (err.response !== undefined) {
    status = err.response.status;

    if (status === NOT_FOUND) {
      res.redirect('/errors/404');
      return;
    }

    if (status === UNAUTHORIZED) {
      res.redirect(`/logout?redirect=${encodeURIComponent(req.originalUrl)}`);
      return;
    }
    status = err.response.status;
    error += `code: ${err.response.status}<br>`;
    error += `code name: ${err.response.statusText}<br>`;
    error += `data: ${JSON.stringify(err.response.data)}`;
    title = model.constants.messages.ERROR;
  }
  res.status(status).render('errors/error', {
    title,
    error,
    ...((req.user === undefined || !req.user.isAuthorized) && {
      layout: 'layout/preLogin',
    }),
  });
};
