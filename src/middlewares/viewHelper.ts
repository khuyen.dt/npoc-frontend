/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * ViewHelperミドルウェア
 */
import {NextFunction, Request, Response} from 'express';
import * as qs from 'qs';
import {labels, valueLst} from '../constants';

// tslint:disable-next-line:max-func-body-length
export default async (req: Request, res: Response, next: NextFunction) => {
  res.locals.valueLst = valueLst;

  /*
   * ページング
   */
  // tslint:disable:no-magic-numbers
  const PAGING_DIVISER = 4;
  const pageLine = 20;
  res.locals.PAGE_LINE = pageLine;
  let maxPageButton = 5;
  if (process.env.MAX_PAGE_BUTTON !== undefined) {
    maxPageButton = parseInt(process.env.MAX_PAGE_BUTTON, 10);
  }
  const searchResultCount = (
    start: string | number,
    end: string | number,
    total: string | number,
  ) => `${total} 件中 ${start} から ${end} まで表示`;

  // tslint:disable-next-line:cyclomatic-complexity
  const pagination = (totalCount: number) => {
    let offset = 0;
    if (req.query.offset !== undefined) {
      offset = parseInt(req.query.offset, 10);
    }
    let html = '';
    if (totalCount > 0) {
      html = `
        <div class="paginate-info">
          ${searchResultCount(
            (offset + 1).toString(),
            (offset + pageLine > totalCount
              ? totalCount
              : offset + pageLine
            ).toString(),
            totalCount.toString(),
          )}
        </div>
      `;
      const breakPaging = Math.floor(totalCount / pageLine) > maxPageButton + 2;
      const breakCount = [];
      if (breakPaging) {
        breakCount.push(Math.floor(maxPageButton / PAGING_DIVISER));
        breakCount.push(Math.floor((maxPageButton - 1) / PAGING_DIVISER));
      }
      html += `<div class="dataTables_paginate paging_simple_numbers" style="float: right"><ul class="pagination">`;
      const query = {...req.query};
      query.pageLine = pageLine;
      let href = 'javascript:void(0)';
      let className = ' disabled';
      if (offset !== 0) {
        query.offset = 0;
        href = `${req.baseUrl}?${qs.stringify(query)}`;
        className = '';
      }
      html += `<li class="paginate_button previous${className}"><a href="${href}">${labels.FIRST}</a></li>`;
      href = 'javascript:void(0)';
      if (offset !== 0) {
        query.offset = offset - pageLine;
        href = `${req.baseUrl}?${qs.stringify(query)}`;
      }
      html += `<li class="paginate_button next${className}"><a href="${href}">${labels.PREVIOUS}</a></li>`;
      for (let i = 0; i < totalCount; i += pageLine) {
        const ignoreCondition =
          (offset >
            breakCount[0] * pageLine + breakCount[1] * pageLine + pageLine ||
            i >= breakCount[0] * pageLine + breakCount[1] * pageLine * 2) &&
          (offset + pageLine <
            totalCount -
              breakCount[0] * pageLine -
              breakCount[1] * pageLine -
              pageLine ||
            i <=
              totalCount -
                breakCount[0] * pageLine -
                breakCount[1] * pageLine * 2 -
                1) &&
          ((i >= breakCount[0] * pageLine &&
            i < offset - breakCount[1] * pageLine) ||
            (i > offset + breakCount[1] * pageLine &&
              i < totalCount - breakCount[0] * pageLine));
        if (breakPaging && ignoreCondition) {
          const temp = `<li class="paginate_button disabled"><a href="javascript:void(0)">...</a></li>`;
          if (html.slice(html.length - temp.length, html.length) !== temp) {
            // 追加したかどうか確認する
            html += temp;
          }
        } else {
          query.offset = i;
          const queryString = qs.stringify(query);
          if (i === offset) {
            html += `
              <li class="paginate_button active"><a href="javascript:void(0)">${i /
                pageLine +
                1}</a></li>
            `;
          } else {
            html += `
            <li class="paginate_button"><a href="${
              req.baseUrl
            }?${queryString}">${i / pageLine + 1}</a></li>
            `;
          }
        }
      }
      href = 'javascript:void(0)';
      className = ' disabled';
      if (offset + pageLine < totalCount) {
        query.offset = offset + pageLine;
        href = `${req.baseUrl}?${qs.stringify(query)}`;
        className = '';
      }
      html += `<li class="paginate_button${className}"><a href="${href}">${labels.NEXT}</a></li>`;
      href = 'javascript:void(0)';
      if (offset + pageLine < totalCount) {
        query.offset = Math.floor((totalCount - 1) / pageLine) * pageLine;
        href = `${req.baseUrl}?${qs.stringify(query)}`;
      }
      html += `<li class="paginate_button${className}"><a href="${href}">${labels.LAST}</a></li>`;
      html += `</ul></div>`;
    } else {
      html = `
        <div class="paginate-info">
          検索結果はありません。
        </div>
      `;
    }
    return html;
  };
  res.locals.pagination = pagination;

  /*
   * string truncate
   */
  let length = 20;
  if (process.env.MIN_TRUNCATE_LENGTH !== undefined) {
    length = parseInt(process.env.MIN_TRUNCATE_LENGTH, 10);
  }
  const truncate = (s: string | null | undefined) => {
    if (s !== null && s !== undefined && s.length > length) {
      return `${s.substr(0, length)}...`;
    } else {
      return s;
    }
  };
  res.locals.truncate = truncate;

  /**
   * nl2br
   */
  function nl2br(str: string, isXhtml = false) {
    if (str === undefined || str === null) {
      return '';
    }
    const breakTag = isXhtml ? '<br />' : '<br>';
    return str.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, `$1${breakTag}$2`);
  }
  res.locals.nl2br = nl2br;

  /**
   * function to set checkbox checked state based on user's search condition
   * @param searchQuery list of option which user checked when doing search
   * @param optionValue the target checkbox's value, to be check
   * @param isDefault is this checkbox default to be checked
   */
  function isChecked(
    searchQuery: object | string | string[] | undefined,
    optionValue: string,
    isDefault = false,
  ) {
    if (searchQuery === undefined) {
      return isDefault ? 'checked' : '';
    } else if (
      searchQuery instanceof Array &&
      searchQuery.indexOf(optionValue) >= 0
    ) {
      return 'checked';
    } else if (searchQuery === optionValue) {
      return 'checked';
    } else {
      return '';
    }
  }
  res.locals.isChecked = isChecked;

  /**
   * function to set dropdownlist selected state based on user's search condition
   * @param searchQuery list of option which user checked when doing search
   * @param optionValue the target option's value, to be check
   * @param isDefault is this option default to be selected
   */
  function isSelected(
    searchQuery: string | string[] | undefined,
    optionValue: string,
    isDefault = false,
  ) {
    if (searchQuery === undefined) {
      return isDefault ? 'selected' : '';
    } else if (
      searchQuery instanceof Array &&
      searchQuery.indexOf(optionValue) >= 0
    ) {
      return 'selected';
    } else if (searchQuery === optionValue) {
      return 'selected';
    } else {
      return '';
    }
  }
  res.locals.isSelected = isSelected;
  next();
};

/**
 * trim spaces in search queries
 * @param searchQuery list of search strings
 */
export const trimQueries = (searchQuery: any) => {
  const clone = {...searchQuery};

  for (const key of Object.keys(clone)) {
    if (clone[key]) {
      clone[key] = clone[key].trim();
    }
  }

  return clone;
};

/**
 * convert query string to api filter object
 * @param req request
 * @return fitler object
 */
export const convertFilterQuery = (query: any, pageLine: number) => {
  // [無効化フラグ] deleted
  if (query && query.deleted && query.deleted instanceof Array) {
    query.deleted = {
      inq: query.deleted,
    };
  }

  // [区分] deleted
  if (query && query.type && query.type instanceof Array) {
    query.type = {
      inq: query.type,
    };
  }

  // search like name 顧客姓, 顧客名, 顧客姓(カナ), 顧客名(カナ)
  if (query && query.name) {
    query.name = {
      like: `%${query.name}%`,
    };
  }

  if (query && query.nameKana) {
    query.nameKana = {
      like: `%${query.nameKana}%`,
    };
  }

  if (query && query.lastName) {
    query.lastName = {
      like: `%${query.lastName}%`,
    };
  }

  if (query && query.firstName) {
    query.firstName = {
      like: `%${query.firstName}%`,
    };
  }

  if (query && query.firstNameKana) {
    query.firstNameKana = {
      like: `%${query.firstNameKana}%`,
    };
  }

  if (query && query.lastNameKana) {
    query.lastNameKana = {
      like: `%${query.lastNameKana}%`,
    };
  }

  if (query && query.tel) {
    query.tel = {
      like: `%${query.tel}%`,
    };
  }

  if (query && query.frameNumber) {
    query.frameNumber = {
      like: `%${query.frameNumber}%`,
    };
  }

  if (query && query.carnm) {
    query.carnm = {
      like: `%${query.carnm}%`,
    };
  }

  if (query && query.mk) {
    query.mk = {
      like: `%${query.mk}%`,
    };
  }

  const filter = {
    where: {
      ...query,
      pageNo: query.offset ? query.offset / pageLine + 1 : 1,
      pageLine: pageLine,
    },
  };

  return filter;
};

/**
 * display database field under text instead of its number value
 * 1 -> value 1
 * 2 -> value 2
 * @param value
 * @param valueLst
 */
export const valueToText = (
  value: string | number | undefined,
  valueLst: any,
) => {
  if (value === null || value === undefined) {
    return value;
  } else {
    return valueLst[value];
  }
};

/**
 * return null if string is empty
 * @param value string
 */
export const emptyStringAsNull = (value: string) =>
  value.length > 0 ? value : null;
