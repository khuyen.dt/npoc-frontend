/**
 * ユーザーミドルウェア
 */
import * as configEnv from 'dotenv';
import {NextFunction, Request, Response} from 'express';
import {UNAUTHORIZED} from 'http-status';

configEnv.config();

export default async (req: Request, res: Response, next: NextFunction) => {
  const apiEndpoint = process.env.API_ENDPOINT || 'http://3.115.209.0/api/';
  const userSession = req.session === undefined ? undefined : req.session.user;
  if (userSession === undefined) {
    req.user = {
      id: undefined,
      userName: undefined,
      token: undefined,
      isAuthorized: false,
      getServiceOption: () => ({
        endpoint: <string>apiEndpoint,
      }),
      destroy: () => undefined, // 何もしない
    };

    if (req.xhr) {
      res.status(UNAUTHORIZED).json({success: false, message: 'unauthorized'});
    } else {
      next();
    }
  } else {
    req.user = {
      ...userSession,
      isAuthorized: true,
      getServiceOption: () => ({
        endpoint: apiEndpoint as string,
        accessToken: userSession.token.accessToken,
        refreshToken: userSession.token.refreshToken,
        storeToken: (token: {refreshToken: string; accessToken: string}) => {
          userSession.token = token;
        },
      }),
      destroy: () => {
        (<Express.Session>req.session).user = undefined; // ユーザーセッションを削除
      },
    };
    res.locals = {
      user: req.user,
      logoutRedirect: encodeURIComponent(req.originalUrl),
    }; // レイアウトに使ってる
    next();
  }
};
