/**
 * 認証ミドルウェア
 */

import {NextFunction, Request, Response} from 'express';

export default async (req: Request, res: Response, next: NextFunction) => {
  if (req.user.role === 0) {
    if (req.originalUrl.toLocaleLowerCase().indexOf('order') <= -1) {
      res.redirect('/order/search');
    } else {
      next();
    }
  } else {
    next();
  }
};
