/**
 * Login controller
 */
import * as model from 'npoc-model';
import {Request, Response, NextFunction} from 'express';
import {messageTypes} from '../constants';
import * as client from '../services';
import {convertFilterQuery} from '../middlewares/viewHelper';
import {commonErrorProcess} from '../middlewares/errorHandler';
import {Customer} from 'npoc-model/lib/models';

const message = model.constants.messages;

/**
 * GET customer
 */
export const index = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const salesService = new client.service.Sales(req.user.getServiceOption());

    const {message} = req.consumeSession();

    const filter = convertFilterQuery(
      {deleted: req.query.deleted || '0', ...req.query},
      res.locals.PAGE_LINE,
    );
    const result = await salesService.salesSearch({filter});

    res.render('sales/index', {
      data: result.res,
      search: req.query,
      dataCount: result.searchNo,
      userLogin: req.user,
      message,
    });
  } catch (err) {
    next(err);
  }
};

/**
 * Get Add Sales Page
 */
export const add = async (req: Request, res: Response) => {
  try {
    let customer = <Customer>{};
    if (req.params.customerId) {
      const option = req.user.getServiceOption();
      const customerService = new client.service.Customer(option);
      customer = await customerService.customerSearchId(req.params.customerId);
    }
    res.render('sales/add', {
      sales: '',
      customer: {
        ...customer,
        name: req.params.customerId
          ? `${customer.lastName}${customer.firstName}`
          : '',
        nameKana: req.params.customerId
          ? `${customer.lastNameKana}${customer.firstNameKana}`
          : '',
      },
      car: {
        id: '',
      },
      companyId: '',
    });
  } catch (err) {
    commonErrorProcess(err, req, res);

    res.render('sales/add', {
      customer: {},
      message: {
        type: messageTypes.error,
        content: err.message,
      },
    });
  }
};

/**
 * Post Add New Sales
 */
export const addProcess = async (req: Request, res: Response) => {
  try {
    const option = req.user.getServiceOption();
    const salesService = new client.service.Sales(option);
    const params = {
      ...req.body,
      customer: {
        id: req.body.customer.id,
      },
      car: {
        id: req.body.car.id,
      },
      sales: {
        ...req.body.sales,
      },
    };
    await salesService.salesCreate(params);

    (<Express.Session>req.session).message = {
      type: messageTypes.success,
      content: message.SAVE_SUCCESS,
    };
    res.redirect('/sales');
  } catch (err) {
    commonErrorProcess(err, req, res);

    let errorMsg = message.SAVE_ERROR;
    if (err.response.status === 422) {
      errorMsg = err.response.message;
    }
    res.render('sales/add', {
      companyId: '',
      ...req.body,
      message: {
        type: messageTypes.error,
        content: errorMsg,
      },
      returnUrl: req.query.returnUrl,
    });
  }
};

/**
 * Get Edit Sales Page
 */
export const edit = async (req: Request, res: Response) => {
  try {
    const option = req.user.getServiceOption();
    const salesService = new client.service.Sales(option);
    const customerService = new client.service.Customer(option);

    const sales = await salesService.salesSearchId(req.params.id);
    const customer = await customerService.customerSearchId(sales.customerId);

    res.render('sales/edit', {
      sales,
      customer: {
        ...customer,
        name: customer.firstName
          ? customer.firstName
          : '' + customer.lastName
          ? customer.lastName
          : '',
        nameKana: customer.firstNameKana
          ? customer.firstNameKana
          : '' + customer.lastNameKana
          ? customer.lastNameKana
          : '',
      },
      car: {
        id: sales.carId,
      },
      companyId: sales.companyId,
    });
  } catch (err) {
    res.render('sales/edit', {
      sales: {},
      customer: {},
      car: {},
      companyId: '',
      message: {
        type: messageTypes.error,
        content: message.ERROR,
      },
    });
  }
};

/**
 * Edit Sales
 */
export const editProcess = async (req: Request, res: Response) => {
  try {
    const option = req.user.getServiceOption();
    const salesService = new client.service.Sales(option);
    const params = {
      ...req.body,
      customer: {id: parseInt(req.body.customer.id)},
      sales: {
        ...req.body.sales,
        type: 1,
        salesDate: '2020-01-01',
        paymentMethod: 1,
        plannedSalesPriceWithTax: 1000,
        plannedSalesPriceWithoutTax: 667,
        carSgaRepairSumWithTax: 1000,
        carSgaRepairSumWithoutTax: 667,
        salesAmountWithTax: 1000,
        salesAmountWithoutTax: 667,
        discount: 1,
        lastPaymentYear:
          req.body.sales.lastPaymentYear !== ''
            ? parseInt(req.body.sales.lastPaymentYear)
            : undefined,
        lastPaymentMonth:
          req.body.sales.lastPaymentMonth !== ''
            ? parseInt(req.body.sales.lastPaymentMonth)
            : undefined,
        lastPaymentTimes:
          req.body.sales.lastPaymentTimes !== ''
            ? parseInt(req.body.sales.lastPaymentTimes)
            : undefined,
      },
      car: {
        id: parseInt(req.body.car.id),
      },
      companyId: parseInt(req.body.companyId),
    };
    const sales = await salesService.salesUpdate(req.params.id, params);

    if (sales.id) {
      res.render('sales/edit', {
        ...req.body,
        message: {
          type: messageTypes.success,
          content: message.SAVE_SUCCESS,
        },
        returnUrl: req.query.returnUrl,
      });
    }
  } catch (err) {
    // debug(err.message);
    res.render('sales/edit', {
      ...req.body,
      message: {
        type: messageTypes.error,
        content: message.SAVE_ERROR,
      },
      returnUrl: req.query.returnUrl,
    });
  }
};
