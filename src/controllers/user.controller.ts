/**
 * Login controller
 */
import * as model from 'npoc-model';
import {Request, Response} from 'express';
import {UNAUTHORIZED} from 'http-status';
import * as client from '../services';

/**
 * GET login
 */
export const login = (_: Request, res: Response) => {
  res.render('login/index', {
    layout: 'layout/preLogin',
    message: '',
    loginId: '',
  });
};

/**
 * POST login
 */
export const auth = async (req: Request, res: Response) => {
  const option = req.user.getServiceOption();
  const userService = new client.service.User(option);

  try {
    // Call API Service
    const result = await userService.login(req.body);

    // Login success
    (req.session as Express.Session).user = {
      ...result.user,
      token: {accessToken: result.token},
    };

    if (
      req.query.redirect !== undefined &&
      req.query.redirect.length > 0 &&
      req.query.redirect !== '/'
    ) {
      res.redirect(decodeURIComponent(req.query.redirect));
    } else {
      res.redirect('/');
    }
  } catch (err) {
    if (err.response.status === UNAUTHORIZED) {
      res.render('login/index', {
        layout: 'layout/preLogin',
        message:
          err.response.data.errors[0].message ||
          model.constants.messages.LOGIN_FAIL,
      });
    } else {
      res.render('login/index', {
        layout: 'layout/preLogin',
        message: err.message || model.constants.messages.LOGIN_FAIL,
      });
    }
  }
};

/**
 * GET logout
 */
export const logout = async (req: Request, res: Response) => {
  try {
    const option = req.user.getServiceOption();
    const userService = new client.service.User(option);
    await userService.logout(option.refreshToken!);
  } catch (err) {
    // // debug(err.message);
  }
  req.user.destroy();
  let redirectURL = '/login';
  if (req.query.redirect !== undefined) {
    redirectURL += `?redirect=${encodeURIComponent(req.query.redirect)}`;
  }
  res.redirect(redirectURL);
};

/**
 * Search User
 */
// export const userSearch = async (req: Request, res: Response, next: NextFunction) => {
//   try {
//     const option = req.user.getServiceOption();
//     const userService = new client.service.User(option);

//     const query = req.query;

//     // Call API Service
//     const result = await userService.userSearch(trimQueries(query));

//     res.render('user/index', {
//       moment: moment,
//       accounts: result.data,
//       searchs: query,
//       message: result.message

//     });
//   } catch (err) {
//     // // debug(err.message);
//     next(err);
//   }
// };

/**
 * Get Add User
 */
export const addUser = async (req: Request, res: Response) => {
  res.render('user/add', {
    account: {loginId: '', userName: '', pass: '', role: ''},
    returnUrl: req.query.returnUrl,
  });
};

/**
 * Post Add User
 */
// export const addProcess = async (req: Request, res: Response) => {
//   try {
//     const option = req.user.getServiceOption();
//     const userService = new client.service.User(option);
//     const result = await userService.userCreate(req.body);

//     if (result.status === status[200]) {
//       const userId = result.data[0].id;
//       (<Express.Session>req.session).message = {
//         type: messageTypes.success,
//         content: message.ICL001
//       };
//       res.redirect(`/user/${userId}`);
//     } else {
//       res.render('user/add', {
//         account: req.body,
//         message: {
//           type: messageTypes.error,
//           content: result.message
//         },
//         returnUrl: req.query.returnUrl,
//         roleLst: api.factory.user.role
//       });
//     }

//   } catch (err) {
//     // debug(err.message);
//     res.render('user/add', {
//       account: req.body,
//       message: {
//         type: messageTypes.error,
//         content: err.message
//       },
//       returnUrl: req.query.returnUrl,
//     });
//   }
// };

/**
 * View User
 */
// export const viewUser = async (req: Request, res: Response, next: NextFunction) => {
//   // tslint:disable-next-line:no-shadowed-variable
//   const { message } = req.consumeSession();
//   try {
//     const option = req.user.getServiceOption();
//     const userService = new client.service.User(option);

//     // Call API Service
//     const result = await userService.userSearchId(<number><unknown>req.params.id);

//     if (result.status === status[200]) {
//       res.render('user/view', {
//         account: result.data[0],
//         message
//       });
//     } else {
//       res.status(NOT_FOUND).render('errors/404');
//     }

//   } catch (err) {
//     // debug(err.message);
//     next(err);
//   }
// };

/**
 * Get Edit User
 */
// export const editUser = async (req: Request, res: Response, next: NextFunction) => {
//   try {
//     const option = req.user.getServiceOption();
//     const userService = new client.service.User(option);

//     // Call API Service
//     const result = await userService.userSearchId(<number><unknown>req.params.id);

//     if (result.status === status[200]) {
//       res.render('user/edit', {
//         account: result.data[0],
//         accountDb: result.data[0],
//         returnUrl: req.query.returnUrl
//       });
//     } else {
//       res.status(NOT_FOUND).render('errors/404');
//     }
//   } catch (err) {
//     // debug(err.message);
//     next(err);
//   }
// };

/**
 * Put Edit User
 */
// export const editProcess = async (req: Request, res: Response) => {
//   const option = req.user.getServiceOption();
//   const userService = new client.service.User(option);
//   const accountInput = { ...req.body };
//   const accountDb = {
//     loginId: req.body.hdnLoginId,
//     role: req.body.hdnRole,
//     pass: req.body.hdnPass,
//     userName: req.body.hdnUserName
//   };
//   try {
//     const account = await userService.userUpdate({
//       id: <number><unknown>req.params.id,
//       loginId: req.body.loginId,
//       userName: req.body.userName,
//       pass: req.body.pass,
//       role: req.body.role
//     });

//     if (account.status === status[200]) {
//       const userId = account.data[0].id;
//       (<Express.Session>req.session).message = {
//         type: messageTypes.success,
//         content: message.EDIT_SUCCESS
//       };
//       res.redirect(`/user/${userId}`);
//     } else {
//       res.render('user/edit', {
//         account: accountInput,
//         accountDb: accountDb,
//         message: {
//           type: messageTypes.error,
//           content: account.message
//         },
//         returnUrl: req.query.returnUrl,
//       });
//     }

//   } catch (err) {
//     // debug(err.message);
//     res.render('user/edit', {
//       account: accountInput,
//       accountDb: accountDb,
//       message: {
//         type: messageTypes.error,
//         content: err.message
//       },
//       returnUrl: req.query.returnUrl,
//     });
//   }
// };
