/**
 * Error Controller
 */
import {Request, Response} from 'express';
import {NOT_FOUND} from 'http-status';

export const notFound = (req: Request, res: Response) => {
  if (req.user.id !== undefined) {
    res.status(NOT_FOUND).render('errors/404');
  } else {
    res.redirect('/login');
  }
};
