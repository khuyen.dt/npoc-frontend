/**
 * Customer Controller
 */
import { NextFunction, Request, Response } from "express";
import * as client from "../../services";
import { convertFilterQuery, valueToText } from "../../middlewares/viewHelper";
import { BAD_REQUEST, UNAUTHORIZED} from 'http-status';
import { Customer } from "npoc-model/lib/models";
import { valueLst } from "../../constants";

export const getCustomer = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const customerService = new client.service.Customer(
      req.user.getServiceOption()
    );

    const filter = convertFilterQuery({...req.query}, req.query.pageLine);
    const result = await customerService.customerSearch({filter});

    result.res = result.res.map((e: Customer) => ({
      ...e,
      deleted: valueToText(e.deleted, valueLst.disableFlgs)
    }));
    res.json(result);
  } catch (err) {
    if (err.response && err.response.status === UNAUTHORIZED) {
      res.status(UNAUTHORIZED).json({ status: 'fail' });
    } else {
      res.status(BAD_REQUEST).json({ status: 'fail' });
    }
  }
};
