/**
 * Customer Controller
 */
import * as client from '../services';
import * as model from 'npoc-model';
import {NextFunction, Request, Response} from 'express';
import {convertFilterQuery} from '../middlewares/viewHelper';
import {messageTypes} from '../constants';
import {commonErrorProcess} from '../middlewares/errorHandler';

const message = model.constants.messages;

/**
 * GET customer
 */
export const index = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const customerService = new client.service.Customer(
      req.user.getServiceOption(),
    );

    const {message} = req.consumeSession();

    const filter = convertFilterQuery(
      {deleted: req.query.deleted || '0', ...req.query},
      res.locals.PAGE_LINE,
    );

    const result = await customerService.customerSearch({filter});

    res.render('customer/index', {
      data: result.res,
      search: req.query,
      dataCount: result.searchNo,
      userLogin: req.user,
      message,
    });
  } catch (err) {
    commonErrorProcess(err, req, res);
    next(err);
  }
};

/**
 * GET customer/add
 */
export const add = async (req: Request, res: Response, next: NextFunction) => {
  try {
    res.render('customer/add', {
      customer: {},
    });
  } catch (err) {
    commonErrorProcess(err, req, res);
    next(err);
  }
};

/**
 * Post customer/add
 */
export const addProcess = async (req: Request, res: Response) => {
  try {
    const option = req.user.getServiceOption();
    const customerService = new client.service.Customer(option);
    await customerService.customerCreate(req.body);

    (<Express.Session>req.session).message = {
      type: messageTypes.success,
      content: message.SAVE_SUCCESS,
    };
    res.redirect('/customer');
  } catch (err) {
    commonErrorProcess(err, req, res);

    let errorMsg = message.SAVE_ERROR;
    if (err.response.status === 422) {
      errorMsg = err.response.message;
    }
    res.render('customer/add', {
      customer: {...req.body},
      message: {
        type: messageTypes.error,
        content: errorMsg,
      },
      returnUrl: req.query.returnUrl,
    });
  }
};
