export const labels = {
  FIRST: '先頭',
  LAST: '最終',
  NEXT: '次',
  PREVIOUS: '前'
};

export const messageTypes = {
  error: 'error',
  info: 'info',
  success: 'success'
};

export interface IMessage {
  // tslint:disable-next-line:no-reserved-keywords
  type: string;
  content: string;
}

export const valueLst = {
  // 無効化フラグ
  disableFlgs: {
    0: '有効',
    1: '無効'
  },
  salesTypes: {
    1: '販売',
    2: '下取販売',
    3: '簡易整備'
  },
  customerTypes: {
    1: '個人顧客',
    2: '法人取引先（顧客・仕入先）',
    3: 'AA取引先'
  },
  sex: {
    1: '男性',
    2: '女性',
    3: 'その他'
  }
}