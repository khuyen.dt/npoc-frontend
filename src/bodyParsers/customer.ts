import * as model from 'npoc-model';
import {replaceEmptyString, replaceNumber} from './common';
import {Request, Response, NextFunction} from 'express';

/**
 * Parse, convert request body for CustomerAdd model
 *  1. Convert type string -> number
 *  2. Convert emptry string -> null
 */
export const customerAdd = (req: Request, _: Response, next: NextFunction) => {
  req.body = replaceEmptyString(req.body, undefined);
  req.body = replaceNumber(req.body, model.specs.CustomerCreate);
  next();
};
