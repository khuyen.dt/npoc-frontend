/* eslint-disable @typescript-eslint/no-unused-vars */
/**
 * INDEX
 */
import * as CustomerBodyParser from './customer';
import * as SalesBodyParser from './sales';

export namespace Customer {
  export import CustomerAdd = CustomerBodyParser.customerAdd;
}

export namespace Sales {
  export import SalesAdd = SalesBodyParser.salesAdd;
}
