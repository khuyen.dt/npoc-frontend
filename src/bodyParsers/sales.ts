import * as model from 'npoc-model';
import {replaceEmptyString, replaceNumber} from './common';
import {Request, Response, NextFunction} from 'express';

/**
 * Parse, convert request body for SalesAdd model
 *  1. Convert type string -> number
 *  2. Convert emptry string -> null
 */
export const salesAdd = (req: Request, _: Response, next: NextFunction) => {
  req.body = replaceEmptyString(req.body, undefined);
  req.body = replaceNumber(req.body, model.specs.SalesCreate);
  req.body.customer = replaceNumber(req.body.customer, model.models.Customer);
  req.body.car = replaceNumber(req.body.car, model.models.Car);
  req.body.sales = replaceNumber(req.body.sales, model.models.Sales);

  next();
};
