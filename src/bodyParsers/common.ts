/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * TODO
 * @param someObj
 * @param replaceValue
 */
export const replaceEmptyString = (someObj: any, replaceValue: any) => {
  const replacer = (key: any, value: any) =>
    String(value) === '' ? replaceValue : value;

  return JSON.parse(JSON.stringify(someObj, replacer));
};

/**
 * TODO
 * @param someObj
 * @param types
 */
export const replaceNumber = (someObj: any, types: any) => {
  const replacer = (key: any, value: any) =>
    typeof types.definition &&
    types.definition.properties[key] &&
    types.definition.properties[key].type == 'number' &&
    value &&
    !isNaN(value)
      ? Number(value)
      : value;

  return JSON.parse(JSON.stringify(someObj, replacer));
};
