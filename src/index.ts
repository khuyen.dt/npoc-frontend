import './LoadEnv';
import { NpocFrontendApplication } from "./application";
import { ApplicationConfig } from "@loopback/core";
import { ExpressServer } from "./server";

export { ExpressServer, NpocFrontendApplication };

export async function main(options: ApplicationConfig = {}) {
  const server = new ExpressServer(options);
  await server.boot();
  await server.start();
}
