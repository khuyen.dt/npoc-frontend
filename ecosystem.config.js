module.exports = {
  apps: [
    {
      name: 'NpocAPI',
      script: 'npm',
      args: 'start',
      cwd: process.env.PWD,

      // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
      // args: 'one two',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],

  deploy: {
    production: {
      user: 'centos',
      host: '3.115.209.0',
      ref: 'origin/master',
      repo: 'git@github.com:repo.git',
      path: '/var/www/npoc',
      'post-deploy':
        'npm ci && pm2 startOrRestart ecosystem.config.js --env production',
    },
    development: {
      user: 'centos',
      host: '3.115.209.0',
      ref: 'origin/develop',
      repo: 'git@gitlab.com:khuyen.dt/npoc-frontend.git',
      path: '/var/www/npoc/web',
      'post-deploy':
        'npm ci && pm2 startOrRestart ecosystem.config.js --env development',
    },
  },
};
