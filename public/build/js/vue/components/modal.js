'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

// register modal component
Vue.component('search-modal', {
  template: '#modal-search-template',
  props: ['header', 'target', 'table', 'type'],
  data: function data() {
    return {
      componentModalInputSearch: this.type + '-input-search',
      paginationContainer: '',
      data: [],
      search: { pageLine: 20, offset: 0, deleted: ['0'] }
    };
  },
  methods: {
    /**
     * Get data from the chosen row
     * Set data back to parent from which it calls the popup
     * @param {*} data 
     */
    setChildData: function setChildData(data) {
      data = JSON.parse(data);
      var target = this.target;
      this.$emit('close');
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = Object.keys(data)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var key = _step.value;

          $(target).find('[name="' + this.type + '[' + key + ']"]').val(data[key]);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    },
    /**
     * Perform search operation
     */
    searchData: function searchData(searchOptions) {
      var _this2 = this;

      this.search = _extends({}, this.search, searchOptions);
      var qs = this.search ? $.param(this.search) : '';
      axios.get('/web/api/' + this.type + '?' + qs).then(function (response) {
        _this2.data = response.data.res;

        // add paging button and info text
        _this2.paginationContainer = pagination(response.data.searchNo, _this2.search.offset, 'searchData');
      }).catch(function (error) {
        _this2.data = [];
        commonAjaxErrorHandler(error.response);
      }).finally(function () {
        return _this2.loading = false;
      });
    }
  },
  created: function created() {},
  mounted: function mounted() {
    this.searchData();
  }
});

Vue.component('customer-input-search', {
  template: '#customer-input-search-template',
  props: ['header', 'table'],
  data: function data() {
    return {
      search: { offset: 1, pageLine: 20, deleted: ['0'] }
    };
  },
  methods: {
    searchData: function searchData() {
      this.search.offset = 0;
      this.$parent.searchData(this.search);
    },
    handleSearchInput: function handleSearchInput(e, name) {
      var isCheckbox = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      if (isCheckbox) {
        if (this.search[name] === null || this.search[name] === undefined) {
          this.search[name] = [];
        }
        if (e.target.checked) {
          this.search[name].push(e.target.value);
        } else {
          this.search[name].splice(this.search[name].indexOf(e.target.value), 1);
        }
      } else {
        this.search[name] = e.target.value;
      }
    },
    isChecked: function isChecked(value) {
      return this.search.deleted == value || this.search.deleted instanceof Array && this.search.deleted.indexOf(value) >= 0;
    }
  }
});

Vue.component('car-input-search', {
  template: '#car-input-search-template',
  props: ['header', 'search', 'table'],
  data: function data() {
    return {};
  },
  methods: {}
});

Vue.component('pagination', {
  props: ['template'],
  template: '<div><slot></slot></div>',
  data: function data() {
    return {};
  },
  updated: function updated() {
    var _this = this;
    this.$nextTick(function () {
      $(this.$el).find('li:not(.disabled):not(.active) a').each(function () {
        var onclickParam = $(this).attr('onclickParam');
        $(this).prop("onclick", null).off("click");
        $(this).bind('click', function () {
          _this.$parent.searchData(JSON.parse(onclickParam));
        });
      });
    });
  }
});

// start app
try {
  new Vue({
    el: '#app',
    data: {
      showModalCar: false,
      showModalCustomer: false
    }
  });
} catch (err) {}