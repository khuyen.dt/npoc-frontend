'use strict';

$(document).ready(function () {
    // var strrole = $('.role-val').val();
    // if (strrole !== undefined) {
    //     $('#select-role').val(strrole);
    // }

    $('#formUser').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    formValidate();

    // Input focusout event
    $('input[name="loginId"], input[name="userName"],' + 'input[name="pass"], select[name="role"]').focusout(function (e) {
        formValidate();
        $(this).valid();
    });

    // 保存 click event
    $("#btnSubmit").click(function (e) {
        e.preventDefault();
        formValidate();

        if ($("#formUser").valid()) {
            formSubmit();
        }
    });

    function formValidate() {
        if ($('#formUser').attr('action').indexOf('add') >= 0) {
            $("#formUser").validate({
                errorClass: 'has-error',
                rules: {
                    loginId: {
                        required: true,
                        maxlength: 255,
                        checkRegexEmail: true,
                        checkRegexCharByte: true
                    },
                    userName: {
                        required: true,
                        maxlength: 20
                    },
                    pass: {
                        required: true,
                        minlength: 8,
                        maxlength: 20,
                        checkRegexCharByte: true
                    },
                    role: {
                        required: true
                    }
                },

                highlight: function highlight(element, errorClass) {
                    $(element).parents('.form-group').addClass(errorClass);
                    $(element).parents().addClass('magrin-bot-0');
                },
                unhighlight: function unhighlight(element, errorClass) {
                    $(element).parents('.form-group').removeClass(errorClass);
                    $(element).parents().removeClass('magrin-bot-0');
                }
            });
        } else {
            $("#formUser").validate({
                errorClass: 'has-error',
                rules: {
                    loginId: {
                        required: true,
                        maxlength: 255,
                        checkRegexEmail: true,
                        checkRegexCharByte: true
                    },
                    userName: {
                        required: true,
                        maxlength: 20
                    },
                    pass: {
                        minlength: 8,
                        maxlength: 20,
                        checkRegexCharByte: true
                    },
                    role: {
                        required: true
                    }
                },

                highlight: function highlight(element, errorClass) {
                    $(element).parents('.form-group').addClass(errorClass);
                    $(element).parents().addClass('magrin-bot-0');
                },
                unhighlight: function unhighlight(element, errorClass) {
                    $(element).parents('.form-group').removeClass(errorClass);
                    $(element).parents().removeClass('magrin-bot-0');
                }
            });
        }
    };

    function formSubmit() {
        $('#btnSubmit').attr('disabled', 'disabled');
        showLoading();
        $("#formUser").submit();
    }
});

function backUrl(returnUrl) {
    if (returnUrl == "") {
        return window.location.href = "/user/search";
    } else {
        return window.location.href = "/" + returnUrl;
    }
}