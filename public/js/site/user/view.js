function backUrl(returnUrl) {
    if (returnUrl == "") {
        return window.location.href = "/user/search";
    }
    else {
        return window.location.href = "/" + returnUrl;
    }
}