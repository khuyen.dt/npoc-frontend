$(document).ready(function () {
    $('#formLogin').validate({
        errorClass: 'has-error',
        rules: {
            loginId: {
              required: true
            },
            pass: {
              required: true
            }
          },
          
        
        highlight:function(element,errorClass){
            $(element).parents('.form-group').addClass(errorClass);
            $('#messIdVal').css('top','0px');
            $('#messPassVal').css('top','0px');
            $('#message').hide()
        },
        unhighlight:function(element,errorClass){
            $(element).parents('.form-group').removeClass(errorClass);
        },

        submitHandler: function(form) {
          $('#btnLogin').attr('disabled','disabled');
          showLoading();
          form.submit();

        }
    })
})