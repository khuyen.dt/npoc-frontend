$(document).ready(function () {
    $('#btn-reset').click(function () {
        $("input[name$='loginId']").val("");
        $("input[name$='userName']").val("");
        $("input[name$='createdAtFrom']").val("");
        $("input[name$='createdAtTo']").val("");
        $("select[name$='role']").val("");
        $("select[name$='disableFlag']").val("");
    });

    var valueDisableFlag = $('#disableFlagVal').val();
    $("select[name='disableFlag']").val(valueDisableFlag);
    var valueRole = $('#roleVal').val();
    $("select[name='role']").val(valueRole);

    for(var i = 1; i <= 4; i++){
        $('#datepicker'+ i).datepicker({
          autoclose: true,
          beforeShow: function () {
            setTimeout(function () {
              $('.ui-datepicker').css('z-index', 99999999999999);
            }, 0);
          },
          showButtonPanel: true,
          changeMonth: true,
          changeYear: true
        })
      }
    
    var table = $('#userSearchTable').removeAttr('width').DataTable({
        "order": [[4, "desc"]],
        // 件数切替機能 非表示
        lengthChange: false,
        // 検索機能 非表示
        searching: false,
        // ソート機能 非表示
        ordering: true,
        // 検索結果 非表示
        // info: false,
        // ページング機能 無効
        // paging: false,
        scrollY: "560px",
        scrollX: true,
        // 縦スクロール時、件数が足りなければheight自動調整
        scrollCollapse: true,
        // 指定した先頭カラムの固定
        // fixedColumns: {
        //   leftColumns: 3
        // }
        iDisplayLength: 50,
        // columnDefs: [
        //   { targets: 'no-sort', orderable: false, visable:false }
        // ],
        language: {
          "decimal": ".",
          "thousands": ",",
          "sProcessing": "処理中...",
          "sLengthMenu": "_MENU_ 件表示",
          "sZeroRecords": "データはありません。",
          // "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
          "sInfo": " _TOTAL_ 件見つかりました",
          // "sInfoEmpty": " 0 件中 0 から 0 まで表示",
          // "sInfoFiltered": "（全 _MAX_ 件より抽出）",
          "sInfoPostFix": "",
          "sSearch": "検索:",
          "sUrl": "",
          "oPaginate": {
            "sFirst": "先頭",
            "sPrevious": "前へ",
            "sNext": "次へ",
            "sLast": "最終"
          }
        },
      });

    $('#btnSearch').on('click', function(e) {
      e.preventDefault();
      var frmUser = $('#frmUser');
      handleSearch(frmUser);      
    });

    $('#frmUser').on('keyup keypress', function (e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) {
        e.preventDefault();
        handleSearch($(this));
      }
    });
});
function handleSearch(frm) {
  $('div.alert-dismissible').remove();
  var createdAtFrom = new Date($('input[name="createdAtFrom"]').val());
  var createdAtTo = new Date($('input[name="createdAtTo"]').val());
  // error message ECL009
  var message = '開始日に終了日以降の日付を入力して検索することはできません。';
  if (createdAtFrom > createdAtTo) {
    createErrorHtml(message);
  } else {
    frm.submit();
  }
}

function createErrorHtml(message) {
  // add error messgae
  var html = "<div class='alert alert-danger alert-dismissible errcustom'>" +
    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" +
    "<h4><i class='icon fa fa-ban'></i> エラー！</h4>" +
    message +
    "</div>";
  $('section.content').prepend(html);
  // scroll back to top page
  window.scrollTo(0, 0);
}