// register modal component
Vue.component('search-modal', {
  template: '#modal-search-template',
  props: [
    'header',
    'target',
    'table',
    'type',
  ],
  data: function () {
    return {
      componentModalInputSearch: `${this.type}-input-search`,
      paginationContainer: '',
      data: [],
      search: {pageLine: 20, offset: 0, deleted: ['0']}
    }
  },
  methods: {
    /**
     * Get data from the chosen row
     * Set data back to parent from which it calls the popup
     * @param {*} data 
     */
    setChildData: function(data) {
      data = JSON.parse(data);
      var target = this.target;
      this.$emit('close');
      for (const key of Object.keys(data)) {
        $(target).find(`[name="${this.type}[${key}]"]`).val(data[key]);
      }
    },
    /**
     * Perform search operation
     */
    searchData(searchOptions) {
      this.search = {
        ...this.search,
        ...searchOptions
      }
      var qs = this.search ? $.param(this.search) : '';
      axios
        .get(`/web/api/${this.type}?` + qs)
        .then(response => {
          this.data = response.data.res;
  
          // add paging button and info text
          this.paginationContainer = pagination(
            response.data.searchNo,
            this.search.offset,
            'searchData'
          );
        })
        .catch(error => {
          this.data = [];
          commonAjaxErrorHandler(error.response);
        })
        .finally(() => this.loading = false)
    }
  },
  created() {
  },
  mounted () {
    this.searchData();
  }
});

Vue.component('customer-input-search', {
  template: '#customer-input-search-template',
  props: [
    'header',
    'table'
  ],
  data: function () {
    return {
      search: { offset: 1, pageLine: 20, deleted: ['0']}
    }
  },
  methods: {
    searchData() {
      this.search.offset = 0;
      this.$parent.searchData(this.search);
    },
    handleSearchInput(e, name, isCheckbox = false) {
      if (isCheckbox) {
        if (this.search[name] === null || this.search[name] === undefined) {
          this.search[name] = [];
        }
        if (e.target.checked) {
          this.search[name].push(e.target.value);
        } else {
          this.search[name].splice(this.search[name].indexOf(e.target.value), 1 );
        }
      } else {
        this.search[name] = e.target.value;
      }
    },
    isChecked(value) {
      return this.search.deleted == value || 
      ((this.search.deleted instanceof Array) && this.search.deleted.indexOf(value) >= 0);
    }
  },
});

Vue.component('car-input-search', {
  template: '#car-input-search-template',
  props: [
    'header',
    'search',
    'table'
  ],
  data: function () {
    return {
    }
  },
  methods: {
  }
});

Vue.component('pagination', {
  props: ['template'],
  template: '<div><slot></slot></div>',
  data: function () {
    return {
    }
  },
  updated: function () {
    var _this = this;
    this.$nextTick(function () {
      $(this.$el).find('li:not(.disabled):not(.active) a').each(function() {
        var onclickParam = $(this).attr('onclickParam');
        $(this).prop("onclick", null).off("click");
        $(this).bind('click', function() {
          _this.$parent.searchData(JSON.parse(onclickParam));
        });
      });
    })
  }
});

// start app
try {
  new Vue({
    el: '#app',
    data: {
      showModalCar: false,
      showModalCustomer: false,
    }
  });
} catch(err) {}
