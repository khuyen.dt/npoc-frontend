/*------------------------------



------------------------------*/

$(function () {


	if($("html").hasClass("pc")){
		$("#drowerMenu").height($("body").height() - 50)
	}
	



	
	$("#menuBtn").on("click",function(evt){
		evt.preventDefault();
		evt.stopPropagation();
		$("body").toggleClass("menuon");
	});

    $(window).scroll(function (evt) {
       
    });
    
    $(window).resize(function () {
        //
    });

    var table = $('#searchTable1').removeAttr('width').DataTable({
        // 件数切替機能 表示
        lengthChange: true,
        // 検索機能 非表示
        searching: false,
        // ソート機能 非表示
        ordering: false,
        // 検索結果 非表示
        // info: false,
        // ページング機能 無効
        // paging: false,
        scrollY: "300px",
        scrollX: true,
        // 縦スクロール時、件数が足りなければheight自動調整
        scrollCollapse: true,
        // 指定した先頭カラムの固定
        // fixedColumns: {
        //   leftColumns: 3
        // }
        language: {
          "decimal": ".",
          "thousands": ",",
          "sProcessing": "処理中...",
          "sLengthMenu": "_MENU_ 件表示",
          "sZeroRecords": "データはありません。",
          "sInfo": " _TOTAL_ 件中 _START_ 件から _END_ 件まで表示",
          // // "sInfo": " _TOTAL_ 件見つかりました",
          "sInfoEmpty": " 0 件中 0件 から 0件 まで表示",
          "sInfoFiltered": "（全 _MAX_ 件より抽出）",
          "sInfoPostFix": "",
          "sSearch": "検索:",
          "sUrl": "",
          "oPaginate": {
            "sFirst": "先頭",
            "sPrevious": "前へ",
            "sNext": "次へ",
            "sLast": "最終"
          }
        },
      });
});

