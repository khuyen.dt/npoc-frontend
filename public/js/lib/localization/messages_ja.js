(function (factory) {
	if (typeof define === "function" && define.amd) {
		define(["jquery", "../jquery.validate"], factory);
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory(require("jquery"));
	} else {
		factory(jQuery);
	}
}(function ($) {

	var ECL001 = 'は入力必須項目です。';
	var ECL006 = 'は半角英数字で入力してください。';
	var ECL009 = 'は半角数字で入力してください。';
		
	/*
	 * Translated default messages for the jQuery validation plugin.
	 * Locale: JA (Japanese; 日本語)
	 */
	$.extend($.validator.messages, {
		required: function (param, input) {
			return $(input).data('name') + ECL001;
		},
		maxlength: function (param, input) {
			return $(input).data('name') + "は「" + $(input).data('maxlength') + "」文字以下で入力してください。（現在「" + $(input).val().length + "」文字)";
		},
		minlength: function(param, input) {
			return $(input).data('name') + "は「" + $(input).data('minlength') + "」文字以上で入力してください。（現在「" + $(input).val().length + "」文字)";
		},
		digits: function (param, input) {
			return $(input).data('name') + ECL009;
		},
		checkCharOneByte: function (param, input) {
			return $(input).data('name') + ECL006;
		},
		isValidDecimalRangeMoisturizing: function (param, input) {
			return $(input).data('name') + "は整数部分「" + $(input).data('maxlengthint') + "」文字, 小数部分「" +$(input).data('maxlengthfloat')+ "」文字以下で入力してください。";
		},

		checkRegexCharByte: function(param, input) {
			return $(input).data('name') + 'では半角英数字および以下の文字以外はご使用いただけません。 (「#」、「$」、「%」、「(」、「) 」、「*」、「+」、「-」、「.」、「/」、「:」、「;」、「?」、「@」、「[」、「]」、「_ 」、「{」、「}」、「~」)';
		},
		isValidDecimalRange: function (param, input) {
			return $(input).data('name') + "は整数部分「" + $(input).data('maxlength1') + "」文字, 小数部分「" +$(input).data('maxlength2')+ "」文字以下で入力してください。";
		},
		remote: "このフィールドを修正してください。",
		email: "有効なEメールアドレスを入力してください。",
		url: "有効なURLを入力してください。",
		date: "有効な日付を入力してください。",
		dateISO: "有効な日付（ISO）を入力してください。",
		number: "有効な数字を入力してください。",
		creditcard: "有効なクレジットカード番号を入力してください。",
		equalTo: "同じ値をもう一度入力してください。",
		extension: "有効な拡張子を含む値を入力してください。",
		rangelength: $.validator.format("{0} 文字から {1} 文字までの値を入力してください。"),
		range: $.validator.format("{0} から {1} までの値を入力してください。"),
		step: $.validator.format("{0} の倍数を入力してください。"),
		max: $.validator.format("{0} 以下の値を入力してください。"),
		min: $.validator.format("{0} 以上の値を入力してください。")
	});
	return $;
}));