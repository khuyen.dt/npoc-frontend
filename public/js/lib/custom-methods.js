$.validator.addMethod('hiragana', function (value) {
  return !(/[^ぁ-ゔゞ゛゜ー]/.test(value));
}, 'ひらがなのみを入力してください。');

$.validator.addMethod('zipcode', function (value, element) {
  return this.optional(element) || /^\d{3}(?:-\d{4})?$/.test(value);
}, '有効な郵便番号を入力してください。');

$.validator.addMethod('checkId', function (_, __, selector) {
  return $(selector).val() !== '';
});

$.validator.addMethod(
  "checkCharOneByte",
  function(value, element) {
      return this.optional(element) || /^[a-zA-Z0-9!-/:-@\\[-`{-~ ]+$/.test(value);
  }
);

$.validator.addMethod(
  "isValidDecimalRangeMoisturizing",
  function(value, element) {
      return this.optional(element) || /^[\d]{1,1}(\.[\d]{1,3})?$/.test(value);
  }
);

$.validator.addMethod(
  "checkRegexEmail",
  function(value, element) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return this.optional(element) || regex.test(value);
}, 'メールアドレスを正しく入力してください。');


$.validator.addMethod(
  "checkRegexCharByte",
  function(value, element) {
    var regex = /^[a-zA-Z0-9#\$\%()\*\+\-\.\/:;?@[\]_{}~]+$/;
    return this.optional(element) || regex.test(value);
});

$.validator.addMethod(
  "isValidDecimalRange",
  function(value, element) {
      return this.optional(element) || /^[\d]{1,1}(\.[\d]{1,2})?$/.test(value);
  }
);
